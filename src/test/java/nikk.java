import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

//Drag and drop
public class nikk {
    public static void main (String args[]) throws InterruptedException, IOException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        Actions ac=new Actions(driver);
        Thread.sleep(1000);
        driver.get("https://jqueryui.com/droppable/");
       Thread.sleep(1000);
        driver.switchTo().frame(driver.findElement(By.className("demo-frame")));
        WebDriverWait wait=new WebDriverWait(driver,10);
        WebElement src=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("draggable")));
       Thread.sleep(1000);
        WebElement des=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("droppable")));
        Thread.sleep(1000);
        ac.dragAndDrop(src,des).perform();
        Thread.sleep(1000);
        File s=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File d=new File("C:\\Users\\nileshdada\\OneDrive\\Desktop\\java program\\tt.jpg");
        FileHandler.copy(s,d);
    }
}
