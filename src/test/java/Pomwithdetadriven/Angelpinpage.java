package Pomwithdetadriven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Angelpinpage {
    @FindBy(xpath = "//*[@id='pin']") private WebElement LoginPin;
    @FindBy(xpath = "//*[@class='button-orange wide']") private WebElement Continueclick;


Angelpinpage (WebDriver driver) {
    PageFactory.initElements(driver, this);
}
public void Enterloginpin(String pin){
   LoginPin .sendKeys(pin);
}
public void ClickonContinueclick(){
    Continueclick.click();
}
}
