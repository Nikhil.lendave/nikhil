package Pomwithdetadriven;

import org.apache.xmlbeans.impl.xb.xsdschema.FieldDocument;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AngelLoginPage {
    //XpathDeclaration
    @FindBy(id = "userid") private WebElement Username;
    @FindBy(id = "password") private WebElement Password;
    @FindBy(xpath = "//*[text()='Login ']") private WebElement LoginButton;
    AngelLoginPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
    public void Enterusername(String username){
        Username.sendKeys(username);

    }
    public void EnterPassword(String password){
        Password.sendKeys(password);
    }
    public void ClickonLoginButton(){
        LoginButton.click();
    }
}
