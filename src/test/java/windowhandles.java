
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Iterator;
import java.util.Set;

public class windowhandles {
    public static void main(String args[]) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://demo.guru99.com/popup.php");
        driver.findElement(By.xpath("//*[text()='Click Here']")).click();
      Set<String> Allwindow=driver.getWindowHandles();//To get ID's of all window
        Iterator<String> It=Allwindow.iterator(); //To get id of each window from set object..+
        String parrent=It.next();
        String child=It.next();
        for (String id: Allwindow){
            driver.switchTo().window(id);
            System.out.println(driver.getTitle());
        }


    }
        }



