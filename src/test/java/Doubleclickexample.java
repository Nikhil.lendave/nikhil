import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.text.Highlighter;

public class Doubleclickexample {
    public static void main(String args[]) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        driver.get("http://demo.guru99.com/test/simple_context_menu.html");
       Thread.sleep(3000);
   WebElement A=driver.findElement(By.xpath("//*[text()='Double-Click Me To See Alert']"));
    Actions ac=new Actions(driver);
    ac.doubleClick(A).perform();
    Alert alt=driver.switchTo().alert();
        String txt=alt.getText();
        System.out.println(txt);
    alt.accept();
    Thread.sleep(3000);


    }
}
