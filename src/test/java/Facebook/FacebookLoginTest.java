package Facebook;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FacebookLoginTest {
    public static void main(String args[]) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        ChromeOptions o=new ChromeOptions();
        o.addArguments("-disable-notifications");
        WebDriver driver=new ChromeDriver(o);
        driver.get("https://www.facebook.com/login.php");
        FBLogin login=new FBLogin(driver);

        login.EnterEmail();
        login.Enterpass();
        login.ClickonLogin();
        Thread.sleep(5000);
        login.ClickonNB(driver);
    }
}
