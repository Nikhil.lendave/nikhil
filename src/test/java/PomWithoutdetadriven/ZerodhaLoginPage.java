package PomWithoutdetadriven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ZerodhaLoginPage {
    public WebDriver driver;
    //xpath Declaration


    @FindBy (id = "userid") private WebElement Username;
@FindBy (id = "password") private WebElement Password;
@FindBy(xpath = "//*[@class='button-orange wide']") private WebElement Loginbutton;
    //Page Factory intilaization
    ZerodhaLoginPage(WebDriver driver){
        PageFactory.initElements(driver,this);

    }

    //Use of methods
    public void enterUsername1(String zerodhausername){

        Username.sendKeys(zerodhausername);
    }
    public void enterPassword1(String pass){


        Password.sendKeys(pass);
    }
    public void clickonLoginbutton(){
        Loginbutton.click();
    }
}


