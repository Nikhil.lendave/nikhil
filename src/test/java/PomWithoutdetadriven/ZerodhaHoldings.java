package PomWithoutdetadriven;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ZerodhaHoldings {



    @FindBy(xpath = "//*[text()='Holdings']") private WebElement Holdings;
    @FindBy(xpath = "(//*[text()='75'])[2]") private WebElement Totalpl;
    @FindBy(xpath = "//*[text()='Holdings']") private WebElement Holding;
    @FindBy(xpath = "//*[text()='Funds']") private WebElement Funds;

   @FindBy(xpath = "//*[@class='symbol'][1]")private WebElement Reliance;
    ZerodhaHoldings(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
    public void ClickonHoldings(){
        Holdings.click();
    }
public void PrintHolding(){
      String H= Holding.getText();
      System.out.println(H);
}
public void ClickonFunds() {
    Funds.click();
}
public void MouseoverReliance(WebDriver driver){
    WebElement element=Reliance;
    Actions act=new Actions(driver);
    act.moveToElement(element).perform();
}
}

