package PomWithoutdetadriven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ZerodhaPinPage {
    //xpathdeclaration
    @FindBy(id = "pin") private WebElement Loginpin;
    @FindBy(xpath = "//*[@class='button-orange wide']") private WebElement Loginclick;
    //Page Factory intilaization
    ZerodhaPinPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
public void enterpin(String zerodhapin) {
    Loginpin.sendKeys(zerodhapin);
}
public void clickoncontiuebutton(){
    Loginclick.click();
    }
}
