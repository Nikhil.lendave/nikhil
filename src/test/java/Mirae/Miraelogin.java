package Mirae;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Miraelogin extends Miraebase {

   WebDriverWait wait=new WebDriverWait(driver,3);
    //Xpath declaration
    @FindBy(xpath = "//*[@id='userid']") private WebElement Username;
    @FindBy(xpath = "//*[@id='password']") private WebElement Password;
    @FindBy(xpath = "//*[text()='Login ']") private WebElement Login;
    Miraelogin(){
        PageFactory.initElements(driver,this);
    }
    public void Enterusername(String username)
    {
        wait.until(ExpectedConditions.visibilityOf(Username)).sendKeys(username);
    }
    public void Enterpassword(String password){
        wait.until(ExpectedConditions.visibilityOf(Password)).sendKeys(password);
    }
    public void Clickonlogin(){
     wait.until(ExpectedConditions.visibilityOf(Login)).click();
    }
}
