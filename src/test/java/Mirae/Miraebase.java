package Mirae;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Miraebase {
    static WebDriver driver;
    @BeforeClass
    public void ss(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("-disable-notifications");
        options.addArguments("__headless");
        driver = new ChromeDriver(options);
        driver.get("https://kite.zerodha.com/");

}
@AfterClass
    public void hh() throws InterruptedException {

driver.quit();
}
}