package poweithoutdetadriven2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.util.logging.FileHandler;

public class FlipLoginPageTest {

    public static void main(String args[]) throws InterruptedException {
        WebDriver driver;
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.get("https://www.flipkart.com/");
        FlipkartLoginPage login=new FlipkartLoginPage(driver);
        login.enterMonumber();
        login.enterPassword();
        login.clickonLogin();
        driver.navigate().back();

    }
}
