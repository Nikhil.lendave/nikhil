package Framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.w3c.dom.Text;

import java.security.cert.X509Certificate;

public class Upstoxloginpage extends Baseclass{
    WebDriverWait wait=new WebDriverWait(driver,3);
    //Xpath Declaration
    @FindBy(id = "userid") private WebElement Username;
    @FindBy(id = "password") private WebElement Password;
    @FindBy(xpath = "//*[@class='button-orange wide']") private WebElement Login;
//@FindBy(xpath = "//*[@id='pin']") private WebElement pin;
//@FindBy(xpath = "//*[text()='Continue ']") private WebElement Continue;
    //Initialization
    Upstoxloginpage( ){
        PageFactory.initElements(driver, this);
    }
    public void Enterusername(String username){
        wait.until(ExpectedConditions.visibilityOf(Username)).sendKeys(username);
    }
    public void Enterpassword(String password){
        wait.until(ExpectedConditions.visibilityOf(Password)).sendKeys(password);
    }


   public void Clickonlogin(){

       Login.click();
    }
    //public void Clickoncontinue(){
      //  Continue.click();
   // }
}
