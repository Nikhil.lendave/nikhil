package Framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class upstoxpinpage extends Baseclass{
WebDriverWait wait=new WebDriverWait(driver,3);
    @FindBy(xpath = "//*[@id='pin']") private WebElement Pin;
    @FindBy(xpath = "//*[text()='Continue ']") private WebElement Continue;
    upstoxpinpage(){
        PageFactory.initElements(driver,this);
    }
    public void Enterpin(String pin){
        wait.until(ExpectedConditions.visibilityOf(Pin)).sendKeys(pin);
    }
    public void Clickoncontinue(){
        Continue.click();
    }
}
