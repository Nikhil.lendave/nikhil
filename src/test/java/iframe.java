import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class iframe {
    public static void main (String args[]) throws InterruptedException, IOException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        Thread.sleep(1000);
        driver.get("http://rediff.com");
        driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='moneyiframe']")));
        String abc=driver.findElement(By.id("nseindex")).getText();
        System.out.println(abc);
        Thread.sleep(1000);
        driver.switchTo().defaultContent();
        WebDriverWait wait=new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='SPORTS']"))).click();
        Thread.sleep(1000);
        File s= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File d=new File("C:\\Users\\nileshdada\\OneDrive\\Desktop\\ss\\nk.jpg");
        FileHandler.copy(s,d);
    }
}
